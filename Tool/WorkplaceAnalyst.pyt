import sys
import os


# TODO: [new tool] draw cost path between two points
# TODO: [new tool] distance matrix among seats
# TODO: [new tool] zone overlap

# TODO: [new tool] elevation profile along a path
# TODO: [new tool] visibility along a path - applied when input is line

# TODO: Create toolbar referencing floor plan to real scale.
# TODO: resample ability
# TODO: add default symbolization

my_scripts = os.path.join(os.path.dirname(__file__), "Scripts")
sys.path.append(my_scripts)

import visibility
reload(visibility)
from visibility import VisibilityFromSeats

import accessibility
reload(accessibility)
from accessibility import AccessibilityFromAssets

import territory
reload(territory)
from territory import TerritoryFromAssets

import windowAngle
reload(windowAngle)
from windowAngle import WindowAngularExposure

import walkpathgraph
reload(walkpathgraph)
from walkpathgraph import WalkpathDistance


class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Workplace Analyst"
        self.alias = "WA"

        # List of tool classes associated with this toolbox
        self.tools = [VisibilityFromSeats,
                      AccessibilityFromAssets,
                      TerritoryFromAssets,
                      WindowAngularExposure,
                      WalkpathDistance]
