"""Compute visibility surface from the locations of workstations.
Each workstation is an observer.
"""

import arcpy


class VisibilityFromSeats(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Visibility From Seat"
        self.description = "This tool computes visibility surface for a route."
        self.canRunInBackground = True

    def getParameterInfo(self):
        """Define parameter definitions"""
        param0 = arcpy.Parameter(name="in_raster",
                                 displayName="Input raster",
                                 direction="Input",
                                 datatype="GPRasterLayer")

        param1 = arcpy.Parameter(name="in_point_feature",
                                 displayName="Input point feature",
                                 direction="Input",
                                 datatype="GPFeatureLayer")

        param2 = arcpy.Parameter(name="out_raster",
                                 displayName="Output raster",
                                 direction="Output",
                                 datatype="DERasterDataset")

        param3 = arcpy.Parameter(name="observer_elevation",
                                 displayName="Observer Elevation",
                                 direction="Input",
                                 datatype="GPDouble",
                                 parameterType="Optional")
        param3.value = 1.0

        param4 = arcpy.Parameter(name="z_factor",
                                  displayName="Z factor",
                                  direction="Input",
                                  datatype="GPDouble",
                                  parameterType="Optional")
        param4.value = 1.0

        param5 = arcpy.Parameter(name="outer_radius",
                                  displayName="Outer radius",
                                  direction="Input",
                                  datatype="GPDouble",
                                  parameterType="Optional")
        param5.value = 2000

        params = [param0, param1, param2, param3, param4, param5]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        try:
            if arcpy.CheckExtension("spatial") == "Available":
                arcpy.CheckOutExtension("spatial")
                return True
            else:
                return False
        except Exception:
            return False  # tool cannot be executed

        return True  # tool can be executed

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""

        if parameters[3].valueAsText is None:  # obs elevation
            parameters[3].value = 1

        if parameters[4].valueAsText is None:  # z factor
            parameters[4].value = 1.0

        if parameters[5].valueAsText is None:  # outer radius
            parameters[5].value = 0

        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        # Environment settings
        arcpy.env.overwriteOutput = True

        out_raster = parameters[2].valueAsText

        # Input variables
        in_raster = parameters[0].valueAsText
        in_observer_features = parameters[1].valueAsText
        out_agl_raster = ""
        analysis_type = "FREQUENCY"
        nonvisible_cell_value = "ZERO"
        z_factor = parameters[4].value
        curvature_correction = "CURVED_EARTH"
        refractivity_coefficient = 0.13
        surface_offset = 0
        observer_elevation = parameters[3].value
        observer_offset = 0
        inner_radius = 0
        outer_radius = parameters[5].value
        horizontal_start_angle = 0
        horizontal_end_angle = 360
        vertical_upper_angle = 90
        vertical_lower_angle = -90

        out = arcpy.sa.Visibility(in_raster, in_observer_features,
                                  out_agl_raster, analysis_type,
                                  nonvisible_cell_value,
                                  z_factor, curvature_correction,
                                  refractivity_coefficient, surface_offset,
                                  observer_elevation, observer_offset,
                                  inner_radius, outer_radius,
                                  horizontal_start_angle,
                                  horizontal_end_angle, vertical_upper_angle,
                                  vertical_lower_angle)
        out.save(out_raster)
        return
