"""Compute accessibility surface from the locations of assets.
set env for process area to raster's extent
"""
# TODO: convert raster to TIN

import arcpy


class WindowAngularExposure(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Angular exposure to windows"
        self.description = "This tool computes how widely windows are seen"
        self.canRunInBackground = True

    def getParameterInfo(self):
        """Define parameter definitions"""
        param0 = arcpy.Parameter(name="in_raster",
                                 displayName="Input wall raster",
                                 direction="Input",
                                 datatype=["GPRasterLayer", "GPTinLayer"])

        param1 = arcpy.Parameter(name="in_source_feature",
                                 displayName="Input source feature",
                                 direction="Input",
                                 datatype="GPFeatureLayer")

        param2 = arcpy.Parameter(name="in_source_height_field",
                                 displayName="Input source height field",
                                 direction="Input",
                                 datatype="Field")
        param2.parameterDependencies = [param1.name]

        param3 = arcpy.Parameter(name="in_window",
                                 displayName="Input windows",
                                 direction="Input",
                                 datatype="GPFeatureLayer")

        param4 = arcpy.Parameter(name="in_window_height_field",
                                 displayName="Input window height field",
                                 direction="Input",
                                 datatype="Field")
        param4.parameterDependencies = [param3.name]

        param5 = arcpy.Parameter(name="resolution",
                                 displayName="Resolution",
                                 direction="Input",
                                 datatype="GPDouble",
                                 parameterType="Optional")
        param5.value = 0.3

        params = [param0, param1, param2, param3, param4, param5]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        try:
            arcpy.CheckOutExtension("spatial")
            arcpy.CheckOutExtension("3D")
            return True
        except Exception:
            return False  # tool cannot be executed

        return True  # tool can be executed

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""

        if parameters[5].valueAsText is None:  # resolution
            parameters[5].value = 0.1
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        # Environment settings
        arcpy.env.overwriteOutput = True
        arcpy.env.extent = "MAXOF"

        # Input variables
        barrier = parameters[0].valueAsText
        seat = parameters[1].valueAsText
        source_height_field = parameters[2].valueAsText
        window = parameters[3].valueAsText
        target_height_field = parameters[4].valueAsText
        distance = parameters[5].value
        arcpy.AddMessage(type(distance))

        dataset = self.construct_sight_lines(
            seat, window, barrier,
            source_height_field, target_height_field,
            distance)

        angles = self.calculate_angles(dataset, distance)
        self.add_field(seat, angles, "ang_exp")
        return

    def add_field(self, seat_fc, angles, field_name):
        arcpy.DeleteField_management(seat_fc, [field_name])

        arcpy.AddField_management(seat_fc, field_name, "FLOAT")
        arcpy.AddMessage("dataset {}".format(len(angles)))

        with arcpy.da.UpdateCursor(seat_fc, ["FID", field_name]) as cursor:
            for row in cursor:
                try:
                    row[1] = angles[row[0]]
                    arcpy.AddMessage("FID:{}, angle:{}".format(row[0], row[1]))
                except KeyError:
                    print(row[0])
                    row[1] = 0
                cursor.updateRow(row)

    def calculate_angles(self, dataset, distance):
        # row index
        source = 0
        target = 1
        dist = 2
        azimuth = 3

        # threshold for distance
        thres = float(distance) * 1.2

        angles = dict()
        p1 = dataset.pop(0)
        angles[p1[source]] = 0
        while dataset:
            p2 = dataset.pop(0)
            if p1[source] == p2[source]:
                if p1[target] == p2[target] and abs(p1[dist]-p2[dist]) < thres:
                    # print("do_something", p1, p2)
                    angle = abs(p1[azimuth] - p2[azimuth])
                    angles[p1[source]] += min(angle, 360-angle)

                # else:
                #     print("continuum ends", p1[source], angles[p1[source]])
            else:
                angles[p2[source]] = 0
            p1 = p2
        # print("continuum ends", p1[source], angles[p1[source]])
        return angles

    def construct_sight_lines(
            self, source, target, barrier,
            source_height_field, target_height_field,
            distance):

        arcpy.env.workspace = "in_memory"
        arcpy.env.scratchWorkspace = "in_memory"
        arcpy.env.overwriteOutput = True
        sight_lines = "sight_"

        arcpy.ddd.ConstructSightLines(
            source, target, sight_lines,
            source_height_field, target_height_field,
            "", distance, "OUTPUT_THE_DIRECTION")

        arcpy.ddd.Intervisibility(sight_lines, obstructions=[barrier],
                                  visible_field="visible")
        fields = ['OID_OBSERV', 'OID_TARGET', 'DIST_ALONG', 'AZIMUTH', "visible"]
        dataset = list()
        arcpy.CopyFeatures_management(sight_lines, "C:/WORK/temp/sight_lines.shp")
        with arcpy.da.SearchCursor(
                sight_lines,
                fields,
                where_clause="visible = 1",
                sql_clause=("", "ORDER BY OID_OBSERV, OID_TARGET, DIST_ALONG")) as cursor:
            for row in cursor:
                dataset.append(row)
        return dataset

    def get_points_along_path(self, path_fc, distance):
        arcpy.env.workspace = "in_memory"
        arcpy.env.scratchWorkspace = "in_memory"
        arcpy.env.overwriteOutput = True

        temp_route = "viewshedr_tmp_"
        temp_points = "viewshedp_tmp_"
        scratch_vertices = "viewshedp2_tmp_"

        arcpy.CopyFeatures_management(path_fc, temp_route)

        # Densify the route
        if distance is not None:
            arcpy.Densify_edit(temp_route, "DISTANCE",
                               distance + " Unknown")

        # Extract the points
        arcpy.FeatureVerticesToPoints_management(temp_route,
                                                 scratch_vertices, "ALL")
        descr1 = arcpy.Describe(scratch_vertices)
        sref = descr1.SpatialReference

        arcpy.CreateFeatureclass_management(arcpy.env.workspace, temp_points,
                                            "POINT", "#", "DISABLED",
                                            "DISABLED", sref)

        arcpy.Append_management(scratch_vertices, temp_points,
                                "NO_TEST", "#", "#")
        return temp_points


def test():
    wa = WindowAngularExposure()
    in_raster = "../../dataset/simple_room/room.tif"
    in_point = "../../dataset/simple_room/seats.shp"
    in_window = "../../dataset/simple_room/windows.shp"
    params = [in_raster, in_point, in_window, "0.2"]
    wa.isLicensed()
    wa.execute(params, "")


if __name__ == "__main__":
    test()