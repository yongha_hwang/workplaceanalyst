"""Compute accessibility surface from the locations of assets.
set env for process area to raster's extent
"""

import arcpy


class AccessibilityFromAssets(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Accessibility From Assets"
        self.description = "This tool computes accessibility surface to assets."
        self.canRunInBackground = True

    def getParameterInfo(self):
        """Define parameter definitions"""
        param0 = arcpy.Parameter(name="in_raster",
                                 displayName="Input raster",
                                 direction="Input",
                                 datatype="GPRasterLayer")

        param1 = arcpy.Parameter(name="in_point_feature",
                                 displayName="Input point feature",
                                 direction="Input",
                                 datatype="GPFeatureLayer")

        param2 = arcpy.Parameter(name="out_raster",
                                 displayName="Output raster",
                                 direction="Output",
                                 datatype="DERasterDataset")

        param3 = arcpy.Parameter(name="friction",
                                 displayName="Friction",
                                 direction="Input",
                                 datatype="GPDouble",
                                 parameterType="Optional")
        param3.value = 1.0

        param4 = arcpy.Parameter(name="wall_value",
                                 displayName="Wall Height (ignore raster values no less than this)",
                                 direction="Input",
                                 datatype="GPDouble",
                                 parameterType="Optional")
        param4.value = 4.0

        params = [param0, param1, param2, param3, param4]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        try:
            if arcpy.CheckExtension("spatial") == "Available":
                arcpy.CheckOutExtension("spatial")
                return True
            else:
                return False
        except Exception:
            return False  # tool cannot be executed

        return True  # tool can be executed

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""

        if parameters[3].valueAsText is None:  # friction
            parameters[3].value = 1.0

        if parameters[4].valueAsText is None:  # wall value
            parameters[4].value = 4.0

        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        # Environment settings
        arcpy.env.overwriteOutput = True
        arcpy.env.extent = "MAXOF"

        out_raster = parameters[2].valueAsText

        # Input variables
        in_raster = parameters[0].valueAsText
        in_asset_features = parameters[1].valueAsText

        friction = parameters[3].value
        wall_value = parameters[4].value

        cost_raster = arcpy.sa.SetNull(
            in_raster, in_raster, "VALUE >= " + str(wall_value))

        cost_raster = cost_raster + friction

        out = arcpy.sa.CostDistance(in_asset_features, cost_raster)
        out.save(out_raster)
        return
