"""Compute average distance between locations using corridor structure.
This requires networkx library.
"""

import arcpy


class AccessibilityFromAssets(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Average distance to locations"
        self.description = "This tool computes average distance to other " \
                           "locations"
        self.canRunInBackground = True

    def getParameterInfo(self):
        """Define parameter definitions"""
        param0 = arcpy.Parameter(name="in_point_feature",
                                 displayName="Input locations",
                                 direction="Input",
                                 datatype="GPFeatureLayer")

        param1 = arcpy.Parameter(name="in_line_feature",
                                 displayName="Input corridor structure",
                                 direction="Input",
                                 datatype="GPFeatureLayer")

        param2 = arcpy.Parameter(name="out_field_name",
                                 displayName="Field name of average distance",
                                 direction="Input",
                                 datatype="GPString")

        params = [param0, param1, param2]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True  # tool can be executed

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        # Environment settings
        corridor_fc = ''
        point_fc = ''
        corridor_network = self._get_corridor_network(corridor_fc)
        avg_distances = self._calculate_average_distance(point_fc, corridor_network)
        return

    def _get_corridor_network(self, corridor_fc):
        return 1

    def _calculate_average_distance(self, point_fc, c_network):
        return 1

    def _get_location_junction_match(self, point_fc, corridor_fc):
        return 1
