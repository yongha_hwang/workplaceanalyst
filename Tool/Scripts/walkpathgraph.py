"""Compute average distance from the locations of assets.
"""

import networkx as nx
import arcpy
import os
import numpy as np


def main():
    wp = WalkpathDistance()
    wp.run()


class WalkpathDistance:
    def __init__(self):
        self.label = "Average Distance Between Seats"
        self.description = "This tool computes average distance between seats"
        self.canRunInBackground = True
        self.junctions = list()
        self.path_length_dic = dict()

    def getParameterInfo(self):
        """Define parameter definitions"""
        param0 = arcpy.Parameter(name="walk_path",
                                 displayName="Input walk path",
                                 direction="Input",
                                 datatype="GPFeatureLayer")

        param1 = arcpy.Parameter(name="in_source_feature",
                                 displayName="Input seats",
                                 direction="Input",
                                 datatype="GPFeatureLayer")

        param2 = arcpy.Parameter(name="in_field_name",
                                 displayName="Field name for results",
                                 direction="Input",
                                 datatype="GPString")

        param3 = arcpy.Parameter(name="average_function",
                                 displayName="Average Function",
                                 direction="Input",
                                 datatype="GPString")
        param3.filter.list = ['mean', 'median']

        params = [param0, param1, param2, param3]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True  # tool can be executed

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        corr_fc = parameters[0].valueAsText
        seat_fc = parameters[1].valueAsText
        result_field = parameters[2].valueAsText
        avg_func = parameters[3].valueAsText

        cg = self.convert_walkpath_to_graph(corr_fc)
        seats = self._get_seats(seat_fc)
        self._assign_junction_to_seat(seats, self.junctions)
        self.path_length_dic = self._make_path_length_dic(cg)
        averages = self._calculate_average(seats, func=avg_func)
        self.add_field_with_dict(
            seat_fc, result_field, "FLOAT", "FID", averages)

    def run(self):
        base_dir = "C:/WORK/repo/workplaceanalyst/dataset/curvy_walls2"
        corr_fc_name = "corridor.shp"
        corr_fc = os.path.join(base_dir, corr_fc_name)
        cg = self.convert_walkpath_to_graph(corr_fc)
        assert(nx.is_connected(cg))

        seat_fc = os.path.join(base_dir, "seats.shp")
        seats = self._get_seats(seat_fc)
        print("{} seats loaded".format(len(seats)))
        self._assign_junction_to_seat(seats, self.junctions)
        self.path_length_dic = self._make_path_length_dic(cg)
        averages = self._calculate_average(seats, func='mean')
        self.add_field_with_dict(
            seat_fc, "avg_dist", "FLOAT", "FID", averages)

    def _calculate_average(self, seats, func='mean'):
        averages = dict()
        for seat1 in seats:
            values = list()
            for seat2 in seats:
                if seat1 == seat2:
                    continue
                values.append(self.get_distance(seat1, seat2))
            if func == 'mean':
                averages[seat1.id] = np.mean(values)
            elif func == 'median':
                averages[seat1.id] = np.median(values)
            print(seat1.id, averages[seat1.id])
        return averages

    def convert_walkpath_to_graph(self, fc):
        path_segment = "in_memory/walkpath"
        temp_junctions = "in_memory/junction"

        c_graph = nx.Graph()
        arcpy.FeatureToLine_management([fc], path_segment)
        arcpy.FeatureVerticesToPoints_management(
            path_segment, temp_junctions, "BOTH_ENDS")
        arcpy.DeleteIdentical_management(temp_junctions, ["Shape"])

        oid_field_name = arcpy.Describe(fc).OIDFieldName

        arcpy.env.overwriteOutput = True

        with arcpy.da.SearchCursor(
                temp_junctions, [oid_field_name, "Shape@"]) as cursor:
            for row in cursor:
                self.junctions.append(Junction(str(row[0]), row[1]))

        wp_fields = ["Shape@", "SHAPE@LENGTH", oid_field_name]
        with arcpy.da.SearchCursor(path_segment, wp_fields) as cursor:
            for path in cursor:
                junctions = self._find_end_junctions(path[0], self.junctions)
                c_graph.add_edge(junctions[0].id, junctions[1].id,
                                 weight=path[1])
        return c_graph

    @staticmethod
    def _find_end_junctions(path_shape, junctions):
        """ find two end junctions of a path segment"""
        for junction in junctions:
            junction.dist = junction.shape.distanceTo(path_shape)
        end_junctions = sorted(
            junctions, key=lambda x: x.dist, reverse=False)[0:2]
        return end_junctions

    @staticmethod
    def _find_nearest_junction(seat, junctions):
        """ find the nearest junction from a seat"""
        for junction in junctions:
            junction.dist = junction.shape.distanceTo(seat.shape)
        nearest = sorted(
            junctions, key=lambda x: x.dist, reverse=False)[0]
        return nearest.id

    @staticmethod
    def _get_seats(seat_fc):
        seats = list()
        oid_field_name = arcpy.Describe(seat_fc).OIDFieldName
        fields = [oid_field_name, "SHAPE@"]
        with arcpy.da.SearchCursor(seat_fc, fields) as cursor:
            for row in cursor:
                seat = Seat(row[0], row[1])
                seats.append(seat)
        return seats

    def _assign_junction_to_seat(self, seats, junctions):
        for seat in seats:
            seat.nearest_junction_id = \
                self._find_nearest_junction(seat, junctions)

    @staticmethod
    def _make_path_length_dic(network):
        return nx.shortest_path_length(network, weight='weight')

    def get_distance(self, seat1, seat2):
        jid1 = seat1.nearest_junction_id
        jid2 = seat2.nearest_junction_id
        return self.path_length_dic[jid1][jid2]

    @staticmethod
    def add_field_with_dict(
            fc, field_name, field_type, dict_key_field, data_dict):
        arcpy.DeleteField_management(fc, [field_name])
        arcpy.AddField_management(fc, field_name, field_type)

        with arcpy.da.UpdateCursor(fc, [dict_key_field, field_name]) as cursor:
            for row in cursor:
                try:
                    row[1] = data_dict[row[0]]
                    cursor.updateRow(row)
                except KeyError:
                    print("Missing Key {}".format(row[0]))


class Junction:
    def __init__(self, jid, shape):
        self.id = jid
        self.shape = shape
        self.dist = 0


class Seat:
    def __init__(self, sid, shape):
        self.id = sid
        self.shape = shape
        self.room_type = ''
        self.nearest_junction_id = ''


if __name__ == '__main__':
    print ("start...")
    main()
