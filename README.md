# GlayAn: a GIS toolbox for workplace layout analysis

### Abstract

This study is about the development of GIS-based tools for analyzing workplace layout. This tool, GlayAn, provides a comprehensive visualization and data management tool that designers or analysts can use to evaluate workplace layout.

By developing this tool as a plugin of a GIS package, we can demonstrate several important advantages of conducting layout analysis using GIS such as customizable visualization, easier management and manipulation of complex datasets, the utilization of ready-made front-end, and rich ecosystem. This study will examine the above advantages through practical analysis examples.

GlayAn is a comprehensive set of tools to intended to support the various analytical methods proposed by scholars and practitioners in this field to analyze workplace layout. Currently, GlayAn consists of five core analysis modules: overall visibility, visibility between workstations, closest facility, distance matrix among colleagues, and angular exposure from windows. More modules are under development. Because each module is independent, it is an advantage of this project that anyone can participate and develop their own module easily.

Previously there were software packages used to analyze layouts, but GlayAn is, to the best of our knowledge, the first comprehensive layout analysis package to use GIS. Conventionally, GIS has been thought of as a tool for geographic analysis only on a town or city scale. This toolbox will demonstrate that GIS is also useful for analyzing micro-scale geography such as indoor space layouts.

Glayan should develop further in several directions. The first is to provide users with more modules on demand. Currently only five modules are released to the public, but more modules are under development. The next step is to make this analysis tool work with tools currently favored by data scientists such as Jupiter Notebook. This will make it easier for GlayAn to use with other state-of-the-art data tools. Another major challenge is to have GlayAn support more GIS packages.


### Introduction

GlayAn is a set of tools for analyzing workplace layouts using a GIS system and visualizing the results. This analysis and visualization allows designers to evaluate workplace layout in a variety of ways. For example, a designer can evaluate how easily an employee access to their colleagues visually or physically, how much an employee's workstations provides access to windows, and how closely an employee's workstation located to a specific facilities like kitchen or printers.

Previously there were software packages used to analyze layouts including DepthMap (Turner 2001) and SPOT (Markhede, Miranda, and Koch 2010), but GlayAn is the first comprehensive layout analysis package to use GIS to the best of our knowledge. Conventionally, GIS has been thought of as a useful tool only for geographic analysis on a town or city scale.  This toolbox will show that GIS is also useful for analyzing micro-scale geography such as indoor space layouts.

#### Why GIS?

We have developed GlayAn for use in GIS packages. This is because we believe that using a GIS package is useful also for layout analysis. Although GIS is not yet widely used in such small scale, GIS provides several important advantages in layout analysis compared to an independent software package.

* Readily-available spatial modules

The first advantage of using GIS when analyzing layouts is that maturely developed modules for spatial operations can still be used without an extra effort.

Whether it is a geographic analysis on an urban scale or a layout analysis on an architectural scale, both analyses require operations on spatial relationships in common. For example, operations such as measuring the distance between two locations, choosing the nearest location, observing spatial distribution, or checking whether the place is visible at this location are not only required at the urban scale. They are also required for layout analysis.

* Customizable visualization
* Easy data management and manipulation
* Ready-made front-end
* Utilizing existing ecosystem
* Easier maintenance
* Standard data format

### Tools

This toolbox, developed in Python, utilizes ESRI's ArcMap Python toolbox. This toolbox consists of several tools for analyzing workplace layout. Currently, it has five tools.

* Overall visibility
* Visibility from locations
* Angular exposure from windows
* Closest facility
* Distance matrix to colleagues


### Example dataset and analysis



### Future development

Glayan should develop further in several directions. The first is to provide users with more modules on demand. Currently only five modules are released to the public, but more modules are under development. The next step is to make this analysis tool work with tools currently favored by data scientists such as Jupiter Notebook. This will make it easier for GlayAn to use with other state-of-the-art data tools. Finally, the number of GIS packages supported by GlayAn is increased. Currently, it is written to be executable in the GIS package with the highest market share, but the goal is to support the product with the highest market share among the open source GIS package.

(integration to BIM)

### How do I get set up? ###

* Location the zip file to your local drive and unzip.
* Access the pyt file from the ArcCatalog.

### Who do I talk to? ###

* Repo owner
